bai1 = () => {
  let i,
    j,
    dem = 0;
  let content = "";
  for (i = 0; i < 10; i++) {
    for (j = 1; j <= 10; j++) {
      if (i == 0) {
        dem = i * 10 + j;
        if (j == 10) {
          content += dem + " ";
        } else {
          content += "0" + dem + " ";
        }
      } else {
        dem = i * 10 + j;
        content += dem + " ";
      }
    }
    content += "<br>";
  }
  document.getElementById("bai1").innerHTML = content;
};
let arrNumber = [];
themPhanTu = () => {
  let number = document.getElementById("bai1-number").value * 1;
  arrNumber.push(number);
  document.getElementById("mang").innerText = arrNumber;
  document.getElementById("bai1-number").value = "";
};
kiemTraSNT = (number) => {
  for (let i = 2; i <= number / 2; i++) {
    if (number % i == 0) {
      return "1";
    }
  }
  return "0";
};
bai2 = () => {
  let arrNumberSNT = [];
  arrNumber.forEach((number) => {
    if (kiemTraSNT(number) == 0 && number != 1 && number != 0) {
      arrNumberSNT.push(number);
    }
  });

  document.getElementById("bai2").innerText = arrNumberSNT;
};

bai3 = () => {
  let number = document.getElementById("number-bai3").value * 1;
};

bai4 = () => {
  let soN = document.getElementById("bai4-number").value * 1;
  let arrNumberBai4 = [];
  for (let i = 1; i <= soN; i++) {
    if (soN % i == 0) {
      arrNumberBai4.unshift(i);
    }
  }
  document.getElementById("bai4").innerText = arrNumberBai4;
};

bai5 = () => {
  let str = "";
  str = document.getElementById("bai5-number").value;
  let reStr = str.split("").reverse().join("");
  document.getElementById("bai5").innerText = reStr;
};

bai6 = () => {
  let kq = 0,
    sum = 0;
  while (sum <= 100) {
    kq++;
    sum += kq;
  }
  kq--;
  document.getElementById("bai6").innerText = "Số X là : " + kq;
};

bai7 = () => {
  let number = document.getElementById("bai7-number").value * 1;
  let contentHTML = "";
  for (let i = 0; i <= 10; i++) {
    contentHTML += number + " x " + i + " = " + number * i;
    contentHTML += "<br>";
  }
  document.getElementById("bai7").innerHTML = contentHTML;
};

bai8 = () => {
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  let player1 = [];
  let player2 = [];
  let player3 = [];
  let player4 = [];
  while (cards.length > 0) {
    player1.push(cards.shift());
    player2.push(cards.shift());
    player3.push(cards.shift());
    player4.push(cards.shift());
  }
  let contentHTML = "";
  contentHTML += "Player1 : " + player1 + "<br>";
  contentHTML += "Player2 : " + player2 + "<br>";
  contentHTML += "Player3 : " + player3 + "<br>";
  contentHTML += "Player4 : " + player4 + "<br>";
  document.getElementById("bai8").innerHTML = contentHTML;
};

bai9 = () => {
  let tongSoChan = document.getElementById("bai9-tongSoChan").value * 1;
  let tongSoCon = document.getElementById("bai9-tongSoCon").value * 1;
  let soCho = tongSoChan / 2 - tongSoCon;
  let soGa = 2 * tongSoCon - tongSoChan / 2;
  let contentHTML = "Số chó : " + soCho + "<br>" + "Số gà : " + soGa;
  document.getElementById("bai9").innerHTML = contentHTML;
};

bai10 = () => {
  let str = document.getElementById("bai10-text").value;
  str = str.split(":");
  let soGio = str.shift() * 1;
  let soPhut = str.shift() * 1;
  let kq = 0;
  let gocPhut, gocGio;
  gocPhut = soPhut * 6;
  gocGio = soGio * 30 + soPhut / 2;
  kq = gocGio > gocPhut ? gocGio - gocPhut : gocPhut - gocGio;
  kq = kq > 180 ? 360 - kq : kq;
  document.getElementById("bai10").innerText = kq + " Độ";
};
